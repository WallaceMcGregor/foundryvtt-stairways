const ICON_SIZE = 40
const BORDER_FACTOR = 1.1

/**
 * A helper for drawing a stairway Control Icon
 * @type {PIXI.Container}
 */
export class StairwayControlIcon extends ControlIcon {
  constructor ({ label, textStyle, typeColor = 0x000000, statusColor = 0x000000, ...options } = {}, ...args) {
    const size = StairwayControlIcon.iconSize
    super({ ...options, size }, ...args)

    // options
    this.labelText = label
    this.labelTextStyle = textStyle
    this.typeColor = typeColor
    this.statusColor = statusColor

    // add offset
    const offset = size * 0.5
    this.x -= offset
    this.y -= offset

    this.draw()
  }

  /* -------------------------------------------- */

  /** @override */
  async draw () {
    await super.draw()

    // draw background
    this.bg.clear().beginFill(this.typeColor || 0, 0.4).lineStyle(2, this.statusColor || 0, 1.0).drawRoundedRect(...this.rect, 5).endFill()

    // label
    this.label = this.label || this.addChild(StairwayControlIcon.createLabel(this.labelText, this.labelTextStyle))

    return this
  }

  /* -------------------------------------------- */

  static get scale () {
    return (canvas.dimensions.size || 100) / 100
  }

  /* -------------------------------------------- */

  static get iconSize () {
    return ICON_SIZE * StairwayControlIcon.scale
  }

  /* -------------------------------------------- */

  static get borderSize () {
    return StairwayControlIcon.iconSize * BORDER_FACTOR
  }

  /* -------------------------------------------- */

  /**
     * Create the Label for the Stairway
     * @return {PreciseText}
     * @private
     */
  static createLabel (text, textStyle) {
    // create the text container
    const label = new PreciseText(text, textStyle)
    label.anchor.set(0.5, 0)

    // set postion
    const iconSize = StairwayControlIcon.iconSize
    label.position.set(iconSize * BORDER_FACTOR * 0.5, iconSize * BORDER_FACTOR)

    return label
  }
}
