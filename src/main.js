'use strict'

import { handleTeleportRequestGM, handleTokenSelectRequestPlayer } from './teleport.js'
import { hookModifyDocument, handleModifyEmbeddedDocument } from './dataQuirks.js'
import { injectControls } from './toolbar.js'
import { injectStairways } from './injection.js'
import { performMigrations } from './migration.js'
import { registerSettings } from './settings.js'
import { Stairway } from './Stairway.js'
import { StairwayLayer } from './StairwayLayer.js'

Hooks.once('init', () => {
  registerSettings()
})

Hooks.once('ready', async () => {
  // migrate data and settings
  await performMigrations()
})

Hooks.on('setup', async () => {
  // inject stairway layer / embedded document in hardcoded places
  injectStairways()

  // redirect modifyDocument events for Stairway
  hookModifyDocument()

  // handle own events
  game.socket.on('module.stairways', (message) => {
    const { eventName, data } = message

    if (eventName === 'modifyDocument') {
      handleModifyEmbeddedDocument(data)
    } else if (eventName === 'teleportRequestGM') {
      handleTeleportRequestGM(data)
    } else if (eventName === 'tokenSelectRequestPlayer') {
      handleTokenSelectRequestPlayer(data)
    } else {
      console.error('unknown eventName:', eventName, data)
    }
  })
})
Hooks.on('getSceneControlButtons', (controls) => {
  if (!game.user.isGM) return
  injectControls(controls)
})

Hooks.on('sightRefresh', (sightLayer) => {
  // Stairway Icons
  for (const sw of canvas.controls.stairways.children) {
    sw.visible = !sightLayer.tokenVision || sw.isVisible
  }
})

Hooks.on(`paste${Stairway.embeddedName}`, StairwayLayer.onPasteStairway)

Hooks.on('renderSidebarTab', async (object, html) => {
  if (object instanceof Settings) {
    const details = html.find('#game-details')
    const list = document.createElement('ul')
    list.innerHTML = await renderTemplate('modules/stairways/templates/settings-info.hbs')
    details.append(list.firstChild)
  }
})
