# 0.7.4
- Improve compatibility with layer injection of other modules

# 0.7.3
- Fixed reload loop on data migration (again)

# 0.7.2
- Fixed reload loop on data migration

# 0.7.1
- Added donation and issue report link
- Added data migration when required fields are missing
- Workaround for incompatibility with incomplete layer injection of other modules

# 0.7.0
- FoundryVTT 0.8.x support

# 0.6.0
- Added label text style config
- Improved stairway paste (GM View)

# 0.5.3
- Added sourceData to hooks
- Fixed data in StairwayTeleport hook

# 0.5.2
- Improved teleport within scene performance
- Fixed label text stroke
- Fixed line draw order
- Fixed "position fixed" bug

# 0.5.1
- Added 'hide' shortcut (alt + right click) in player view
- Fixed Icon drawing issue (Cannot read property 'scale' of null)
- Fixed Stairway connection wasn't always updated on configuration change
- Fixed GM teleport between scenes with no token selected
- Fixed other placeables where shown on drag and drop (GM layer)

# 0.5.0
- Improved teleport handling when multiple GMs are connected
- Added Hooks `PreStairwayTeleport` and `StairwayTeleport` for developers

# 0.4.2
- Fixed missing tokens after teleport between scenes

# 0.4.1
- Improved teleport between scenes:
    - no more scene reloading on teleport
    - fixes "No token on scene" warning for players

# 0.4.0
- Added stairway (text) labels
- Added option to disable (lock) stairways
- Added option to hide stairways from players
- Allow stairway usage without a selected token for GM
- Fixed SightLayer not updated on all changes. This should fix missing / missplaced stairway icons for players.

# 0.3.0
- Allow custom stairway icons

# 0.2.0
- Stairways between two scenes
- Colored stairway status in GM tool
- Configuration: move animation instead of teleport within scene

# 0.1.0
First release.